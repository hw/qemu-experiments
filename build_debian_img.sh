#!/bin/bash -ex

ARCH="amd64"
ISO="debian-10.2.0-${ARCH}-netinst.iso"
ISO_URL="https://cdimage.debian.org/debian-cd/current/${ARCH}/iso-cd/${ISO}"
IMAGE="debian-${ARCH}.qcow2"

wget -c "${ISO_URL}"

qemu-img create -f qcow2 ${IMAGE} 20G
# qemu-img create -f qcow2 -o preallocation=falloc ${IMAGE} 20G

qemu-system-x86_64 -enable-kvm -cdrom ${ISO} -hda ${IMAGE} -boot d -m 2G

## ... set up base img
##      - set grub timeout to 0
##      - add fastboot option to grub kernel command lines
##      - add to fstab: 'shared /mnt 9p trans=virtio,version=9p2000.L,rw 0 0'
##      - apt install build-essentials

## clone disposable cow disk from base image:
## qemu-img create -f qcow2 -o backing_file=${IMAGE} test01.img 
## qemu-system-x86_64 -nographic -m 256 -hda test01.img -enable-kvm

